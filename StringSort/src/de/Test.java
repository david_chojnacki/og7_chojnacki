package de;

import java.util.ArrayList;

public class Test {

	public static void main(String[] args) {
		ArrayList<String> Liste = new ArrayList<String>();
		Liste.add("Hey");
		Liste.add("Wie");
		Liste.add("Du");
		Test.bubblesort(Liste);
		System.out.println(Liste);
	}

	public static ArrayList<String> bubblesort(ArrayList<String> zusortieren) {
		String temp;
		for (int i = 1; i < zusortieren.size(); i++) {
			for (int j = 0; j < zusortieren.size() - i; j++) {
				if (zusortieren.get(j).compareTo(zusortieren.get(j+1)) <0) {
					temp = zusortieren.get(j);
					zusortieren.add(j,zusortieren.get(j+1));
					zusortieren.add(j+1, temp);
				}

			}
		}
		return zusortieren;
	}
}
