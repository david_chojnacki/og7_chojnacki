package gui;

public class BorderLayout extends javax.swing.JFrame {

	private javax.swing.JPanel jPanel1 = new javax.swing.JPanel();
	private javax.swing.JButton jButton1 = new javax.swing.JButton("Button #1");
	private javax.swing.JButton jButton2 = new javax.swing.JButton("Button #2");
	private javax.swing.JButton jButton3 = new javax.swing.JButton("Button #3");
	private javax.swing.JButton jButton4 = new javax.swing.JButton("Button #4");
	private javax.swing.JButton jButton5 = new javax.swing.JButton("Button #5");

	public BorderLayout() {

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		jPanel1.setLayout(new java.awt.BorderLayout());

		jPanel1.add(jButton1, java.awt.BorderLayout.PAGE_START);
		jPanel1.add(jButton2, java.awt.BorderLayout.PAGE_END);
		jPanel1.add(jButton3, java.awt.BorderLayout.LINE_START);
		jPanel1.add(jButton4, java.awt.BorderLayout.CENTER);
		jPanel1.add(jButton5, java.awt.BorderLayout.LINE_END);

		this.getContentPane().add(jPanel1);
		pack();
	}

	public static void main(String args[]) {
		new BorderLayout().setVisible(true);
	}

}