package de;

public class Trainer extends Person {

	private char lizenzklasse;
	private int aufwandsentschaedigung;

	public Trainer(String name, char lizenzklasse, int aufwandsentschaedigung) {
		super(name);
		this.lizenzklasse = lizenzklasse;
		this.aufwandsentschaedigung = aufwandsentschaedigung;
	}

	public char getLizenzklasse() {
		return lizenzklasse;
	}

	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}

	public int getAufwandsentschaedigung() {
		return aufwandsentschaedigung;
	}

	public void setAufwandsentschaedigung(int aufwandsentschaedigung) {
		this.aufwandsentschaedigung = aufwandsentschaedigung;
	}

}
