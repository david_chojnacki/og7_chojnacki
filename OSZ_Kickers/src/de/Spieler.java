package de;

public class Spieler extends Person {

	private int telefonnummer;
	private boolean istJahresbetregbezahlt;
	private int trikotnummer;
	private String spielposition;

	public Spieler(String name, int telefonnummer, boolean istJahresbetregbezahlt, int trikotnummer,
			String spielposition) {
		super(name);
		this.telefonnummer = telefonnummer;
		this.istJahresbetregbezahlt = istJahresbetregbezahlt;
		this.trikotnummer = trikotnummer;
		this.spielposition = spielposition;
	}

	public int getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(int telefonnummer) {
		this.telefonnummer = telefonnummer;
	}

	public boolean getIstJahresbetregbezahlt() {
		return istJahresbetregbezahlt;
	}

	public void setIstJahresbetregbezahlt(boolean istJahresbetregbezahlt) {
		this.istJahresbetregbezahlt = istJahresbetregbezahlt;
	}

	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}

	public String getSpielposition() {
		return spielposition;
	}

	public void setSpielposition(String spielposition) {
		this.spielposition = spielposition;
	}

}
