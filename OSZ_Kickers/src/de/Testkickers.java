package de;

public class Testkickers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Spieler meinSpieler = new Spieler("Dante", 1577320481, true, 17, "Mittelfeld");
		Trainer meinTrainer = new Trainer("Dieter", 'A', 150);
		Schiedsrichter meinSchiedsrichter = new Schiedsrichter("Pepe", 20);
		Mannschaftsleiter meinMannschaftsleiter = new Mannschaftsleiter("Samuel", 1357739281, true, 7,"Außenverteidiger", "OSZ TORnados", false);
		
		
		

		System.out.println("Spieler Name : " + meinSpieler.getName() + ", Telefonnummer : "
				+ meinSpieler.getTelefonnummer() + ", Spieler Jahresbetrag bezahlt ? : "
				+ meinSpieler.getIstJahresbetregbezahlt() + ", Spieler Trikotnummer : " + meinSpieler.getTrikotnummer()
				+ ", Spieler Spielposition : " + meinSpieler.getSpielposition());
		

		System.out.println(
				"Trainer Name : " + meinTrainer.getName() + ", Trainer Lizenzklasse " + meinTrainer.getLizenzklasse()
						+ ", Trainer Aufwandsentschaedigung " + meinTrainer.getAufwandsentschaedigung());
		

		System.out.println("Schiedsrichter Name : " + meinSchiedsrichter.getName()
				+ ", Schiedsrichter gepfiffene Spiele : " + meinSchiedsrichter.getGepfiffeneSpiele());
		

		System.out.println("Mannschaftsleiter Name : " + meinMannschaftsleiter.getName()
				+ ", Mannschaftsleiter Telefonnummer : " + meinMannschaftsleiter.getTelefonnummer()
				+ ", Mannschaftsleiter Jahresbetrag bezahlt ? : " + meinMannschaftsleiter.getIstJahresbetregbezahlt()
				+ ", Mannschaftsleiter Trikotnummer : " + meinMannschaftsleiter.getTrikotnummer() + " Spielposition : "
				+ meinMannschaftsleiter.getSpielposition() + ", Teamname : " + meinMannschaftsleiter.getTeamname()
				+ ", Mannschaftsleiter Rabatt ? : " + meinMannschaftsleiter.getIstRabatt());
	}

}
