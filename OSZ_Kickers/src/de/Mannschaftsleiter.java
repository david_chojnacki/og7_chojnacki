package de;

public class Mannschaftsleiter extends Spieler {

	private String teamname;
	private boolean istRabatt;

	public Mannschaftsleiter(String name, int telefonnummer, boolean istJahresbetregbezahlt, int trikotnummer,
			String spielposition, String teamname, boolean istRabatt) {
		super(name, telefonnummer, istJahresbetregbezahlt, trikotnummer, spielposition);
		this.teamname = teamname;
		this.istRabatt = istRabatt;
	}

	public String getTeamname() {
		return teamname;
	}

	public void setTeamname(String teamname) {
		this.teamname = teamname;
	}

	public boolean getIstRabatt() {
		return istRabatt;
	}

	public void setIstRabatt(boolean istRabatt) {
		this.istRabatt = istRabatt;
	}

}
