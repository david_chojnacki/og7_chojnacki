package de;

public class Schiedsrichter extends Person {

	private int gepfiffeneSpiele;

	public Schiedsrichter(String name, int gepfiffeneSpiele) {
		super(name);
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}

	public int getGepfiffeneSpiele() {
		return gepfiffeneSpiele;
	}

	public void setGepfiffeneSpiele(int gepfiffeneSpiele) {
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}

}
