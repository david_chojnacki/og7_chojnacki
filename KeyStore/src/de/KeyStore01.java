package de;

public class KeyStore01 {
	private String[] keystore;

	public KeyStore01() {
		this.keystore = new String[100];
	}

	public KeyStore01(int length) {
		this.keystore = new String[length];
	}

	public boolean add(String e) {
		for (int i = 0; i < keystore.length; i++) {
			if (this.keystore[i] == null) {
				this.keystore[i] = e;
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		String s = "";
		for (int i = 0; i < this.keystore.length; i++) {
			if (this.keystore[i] != null)
				s += this.keystore[i] + "\n";
		}
		return s;
	}

	public int indexOf(String str) {
		for (int i = 0;i<this.keystore.length;i++) {
		if(this.keystore[i].equals(str))
			return i;
		}
		return -1;
	}

	public void remove(int index) {
			keystore[index] = null;
			for(; index < this.keystore.length - 1; index++) {
				this.keystore[index] = this.keystore[index+1];
			}
			this.keystore[this.keystore.length-1] = null;
			
	}
}
