import java.util.*;

public class CollectionTest {

	static void menue() {
		System.out.println("\n ***** Buch-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) loeschen");
		System.out.println(" 4) Die groesste ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Buch b1 = new Buch("Richard Helm", "Design Patterns", "ISBN-10:0201633612");
		Buch b2 = new Buch("Robert C. Martin", "Clean Code", "ISBN-10:0132350882");
		Buch b3 = b1;
		List<Buch> buchliste = new LinkedList<Buch>();
		Scanner myScanner = new Scanner(System.in);

		char wahl;
		String eintrag;

		int index;
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':
				buchliste.add(b1);
				buchliste.add(b2);
				buchliste.add(b3);
				break;
			case '2':
				System.out.print("Welche ISBN soll gesucht werden ? ");
				String gesuchteISBN = myScanner.next();
				for (int i = buchliste.size() - 1; i > 0; i--)
					for (int j = buchliste.size() - 1; j > 0; j--)
						if (buchliste.get(i).getIsbn().compareTo(gesuchteISBN) == 0) {
							System.out.println(buchliste.get(i));
							break;
						}
				break;
			case '3':
				System.out.println("Welches Buch soll entfernt werden ?");
				switch (myScanner.next().charAt(0)) {
				case '1':
					buchliste.remove(b1);
					break;
				case '2':
					buchliste.remove(b2);
					break;
				case '3':
					buchliste.remove(b3);
					break;
				default:
					menue();
					wahl = myScanner.next().charAt(0);
				}

				System.out.print(buchliste.toString());
				break;
			case '4':
				for (int i = buchliste.size() - 1; i > 0; i--)
					for (int j = buchliste.size() - 1; j > 0; j--)
						if (buchliste.get(i).compareTo(buchliste.get(j)) > 0)
							System.out.println(buchliste.get(i).getIsbn());

				break;
			case '5':
				System.out.println(buchliste.toString());
				break;
			case '9':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
			} // switch

		} while (wahl != 9);
	}// main

	public static Buch findeBuch(List<Buch> buchliste, String isbn) {
		// add your implementation
		return null;
	}

	// public static boolean loescheBuch(........) {
	// add your implementation
	// }

	// public static String ermitteleGroessteISBN(........) {
	// add your implementation
	// }

}